package org.rusia.tests;

import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.rusia.tests.constants.Constants;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;


public class AlertsTests extends BaseTestClass{
    @Test
    public void clickJSAlert() {
        String textOnAlertExpected = "I am a JS Alert";
        String resultTextExpected = "You successfuly clicked an alert";

        goToUrl(Constants.JAVASCRIPT_ALERTS_URL);
        pressButtonByText("Click for JS Alert");
        String textOnAlertActual = switchToAlertAndGetText(true);
        String resultTextActual = getTextFromResultSection();

        Assert.assertEquals(textOnAlertActual, textOnAlertExpected,
                "Texts on alert is not correct");
        Assert.assertEquals(resultTextActual, resultTextExpected,
                "Result text is not correct");
    }

    public void clickJSAlertUsingJSExecutor() {
        String textOnAlertExpected = "I am a JS Alert";
        String resultTextExpected = "You successfuly clicked an alert";

        goToUrl(Constants.JAVASCRIPT_ALERTS_URL);
        ((JavascriptExecutor)driver).executeScript("return jsAlert");
        //pressButtonByText("Click for JS Alert");
        String textOnAlertActual = switchToAlertAndGetText(true);
        String resultTextActual = getTextFromResultSection();

        Assert.assertEquals(textOnAlertActual, textOnAlertExpected,
                "Texts on alert is not correct");
        Assert.assertEquals(resultTextActual, resultTextExpected,
                "Result text is not correct");
    }


    public void pressButtonByText(String text) {
        driver
                .findElement(By.id("content"))
                .findElement(By.xpath(".//button[text()='" + text + "']"))
                .click();
    }

    public String switchToAlertAndGetText(boolean accept) {
        Alert alert = driver.switchTo().alert();
        String textOnAlertActual = alert.getText();
        if (accept)
            alert.accept();
        else
            alert.dismiss();
        return textOnAlertActual;
    }

    public String getTextFromResultSection() {
        return driver
                .findElement(By.id("result"))
                .getText();
    }

}
