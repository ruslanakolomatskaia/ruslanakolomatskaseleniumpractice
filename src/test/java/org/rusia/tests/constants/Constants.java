package org.rusia.tests.constants;

public class Constants {
    public static final String BASE_URL = "https://the-internet.herokuapp.com/";
    public static final String JAVASCRIPT_ALERTS_URL = BASE_URL + "javascript_alerts";
    public static final String WINDOW_URL = BASE_URL + "windows";

}
