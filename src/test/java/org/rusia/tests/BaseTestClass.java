package org.rusia.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.rusia.driver.BrowserName;
import org.rusia.driver.WebDriverFactory;
import org.rusia.tests.constants.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


public class BaseTestClass {
    protected WebDriver driver = null;

    @BeforeSuite
    public void beforeSuite(){
        driver = WebDriverFactory.initDriver(BrowserName.CHROME);
        driver.manage().window().maximize();
    }

    @AfterSuite
    public void afterSuite(){
        if (driver != null){
            driver.quit();
        }
    }

    public void pause(long msec) {
        try {
            Thread.sleep(msec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void goToUrl(String url){
        driver.get(url);
    }

    public void goToUrl(){
        goToUrl(Constants.BASE_URL);
    }

}
