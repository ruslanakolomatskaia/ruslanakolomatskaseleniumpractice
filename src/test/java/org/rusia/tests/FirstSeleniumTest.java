package org.rusia.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.rusia.tests.constants.Constants;
import org.omg.PortableInterceptor.ServerRequestInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;

public class FirstSeleniumTest extends BaseTestClass {
    String searchParameter = "hillel it school";

    @Test
    public void test() {
        goToUrl("https://google.com.ua");
        By by = By.xpath("//input[@name='q']");
        WebElement searchField = driver.findElement(by);
        searchField.sendKeys(searchParameter);
        searchField.submit();
        pause(5000);
        List<WebElement> links = driver.findElements(By.cssSelector("a > h3"));
        for (WebElement link : links) {
            String text = link.getText();
            Assert.assertTrue(text.contains("Hillel"), "Text in link should contain Hillel");
        }
    }


}

