package org.rusia.tests;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.rusia.tests.constants.Constants;
import org.testng.Assert;
import org.testng.annotations.Test;

public class HomeworkAlerts extends BaseTestClass {
    @Test
    public void clickJSConfirmClickOK() {
        String resultTextExpected = "You clicked: Ok";

        goToUrl(Constants.JAVASCRIPT_ALERTS_URL);
        pressButtonByText("Click for JS Confirm");
        //driver.switchTo().alert().accept();
        switchToAlertAndClickOK(true);
        String resultTextActual = getTextFromResultSection();

        Assert.assertEquals(resultTextActual, resultTextExpected,
                "Result text is not correct");
    }

    @Test
    public void clickJSConfirmClickCancel() {
        String resultTextExpected = "You clicked: Cancel";

        goToUrl(Constants.JAVASCRIPT_ALERTS_URL);
        pressButtonByText("Click for JS Confirm");
        //driver.switchTo().alert().dismiss();
        switchToAlertAndClickOK(false);
        String resultTextActual = getTextFromResultSection();

        Assert.assertEquals(resultTextActual, resultTextExpected,
                "Result text is not correct");
    }

    @Test
    public void clickJSPromptWithTextClickOK() {
        String resultTextExpected = "You entered: text";

        goToUrl(Constants.JAVASCRIPT_ALERTS_URL);
        pressButtonByText("Click for JS Prompt");
        driver.switchTo().alert().sendKeys("text");
        // driver.switchTo().alert().accept();
        switchToAlertAndClickOK(true);
        String resultTextActual = getTextFromResultSection();

        Assert.assertEquals(resultTextActual, resultTextExpected,
                "Result text is not correct");
    }

    @Test
    public void clickJSPromptWithTextClickCancel() {
        String resultTextExpected = "You entered: null";

        goToUrl(Constants.JAVASCRIPT_ALERTS_URL);
        pressButtonByText("Click for JS Prompt");
        driver.switchTo().alert().sendKeys("text");
        // driver.switchTo().alert().dismiss();
        switchToAlertAndClickOK(false);
        String resultTextActual = getTextFromResultSection();

        Assert.assertEquals(resultTextActual, resultTextExpected,
                "Result text is not correct");
    }

    @Test
    public void clickJSPromptWithoutTextClickOK() {
        String resultTextExpected = "You entered: "; //интересно что в chrome кейс проходил без пробела в конце текста, а в сафари фейлился. По факту там действительно пробел

        goToUrl(Constants.JAVASCRIPT_ALERTS_URL);
        pressButtonByText("Click for JS Prompt");
        //driver.switchTo().alert().accept();
        switchToAlertAndClickOK(true);
        String resultTextActual = getTextFromResultSection();

        Assert.assertEquals(resultTextActual, resultTextExpected,
                "Result text is not correct");
    }

    @Test
    public void clickJSPromptWithoutTextClickCancel() {
        String resultTextExpected = "You entered: null";

        goToUrl(Constants.JAVASCRIPT_ALERTS_URL);
        pressButtonByText("Click for JS Prompt");

        //driver.switchTo().alert().dismiss();
        switchToAlertAndClickOK(false);
        String resultTextActual = getTextFromResultSection();

        Assert.assertEquals(resultTextActual, resultTextExpected,
                "Result text is not correct");
    }


    public void pressButtonByText(String text) {
        driver
                .findElement(By.id("content"))
                .findElement(By.xpath(".//button[text()='" + text + "']"))
                .click();
    }

    public void switchToAlertAndClickOK(boolean accept) {
        Alert alert = driver.switchTo().alert();
        if (accept) {
            alert.accept();
        } else {
            alert.dismiss();
        }
    }

    public String getTextFromResultSection() {
        return driver
                .findElement(By.id("result"))
                .getText();
    }

}
